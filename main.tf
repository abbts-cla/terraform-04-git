resource "azurerm_resource_group" "rgdemo" {
  name     = "rg-${var.infix}"
  location = var.location
}

resource "azurerm_virtual_network" "main" {
  name                = "vnet-${var.infix}"
  address_space       = ["10.1.0.0/16"]
  location            = azurerm_resource_group.rgdemo.location
  resource_group_name = azurerm_resource_group.rgdemo.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rgdemo.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.1.2.0/24"]

}

resource "azurerm_network_interface" "main" {
  name                = "${var.infix}-nic"
  location            = azurerm_resource_group.rgdemo.location
  resource_group_name = azurerm_resource_group.rgdemo.name

  ip_configuration {
    name                          = "ifconfig"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_windows_virtual_machine" "vmdemo" {
  name                  = var.infix
  resource_group_name   = azurerm_resource_group.rgdemo.name
  location              = azurerm_resource_group.rgdemo.location
  size                  = "Standard_F2"
  admin_username        = "adminuser"
  admin_password        = "P@$$w0rd1234!"
  patch_assessment_mode = "AutomaticByPlatform"

  identity {
    type = "SystemAssigned"
  }

  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    name                 = "${var.infix}-disk0"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2022-Datacenter"
    version   = "latest"
  }
}
